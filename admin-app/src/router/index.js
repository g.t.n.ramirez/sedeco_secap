import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home'

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "Inicio",
        component: Home
    }
];

var configRouter = {
    mode: 'history',
    routes: routes
  };

export default new VueRouter(configRouter);