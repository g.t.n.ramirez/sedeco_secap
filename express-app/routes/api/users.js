const express = require('express');
const router = express.Router();
const UserServices = require('../../services/users');
const {firebase, admin} = require('../../firebase/firebase')
const chheckIfAuthenticated = require('../../middlewares/auth')

const userServices = new UserServices();


//create admin user
router.post('/admin', async function(req, res, next){
    const { email, password, data} = req.body
    try {
        const user = await admin.auth().createUser({
            email : email,
            password : password,
        }).catch(error =>console.log(error))
        admin.auth().setCustomUserClaims(user.uid, {role : data.role}).then( _ =>{
            firebase.firestore().collection("users").doc(user.uid).set(data)
        })
        res.status(201).json({
            message : 'Usuario registrado'
        })
    }catch (error) {
        console.log(error)
    }
})

router.get('/:uid', async function(req, res, next){
    const { uid } = req.params
    try {
        
        const user = await userServices.getUser(uid)
        let date;
        try {
            date = user.last_purchase.toDate()
        } catch (error) {
            date = user.last_purchase
        }
        
        res.status(200).json({
            user : user,
            last_purchase : date
        });
        
    } catch (error) {
        res.status(500).json({
            data : error
        });
        console.log(error)
    }

})

module.exports = router;