const express = require('express');
const router = express.Router();
const ProductServices = require('../../services/products');

const productServices = new ProductServices();

router.get('/', async function(req, res, next) {
    try {
        const setProduct = await productServices.getProduct();
        res.status(201).json({
            data: setProduct
        });
    } catch (error) {
        res.status(500).json({
            data : "Error"
        });
    }
});

module.exports = router;