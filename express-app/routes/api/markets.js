const express = require('express');
const router = express.Router();
const MarketServices = require('../../services/markets');

const marketServices = new MarketServices();

router.get('/', async function (req, res, next) {
    try {
        
        const markets = await marketServices.getMarkets()
        
        res.status(201).json({
            data: markets
        });
        
    } catch (error) {
        res.status(500).json({
            data : error
        });
    }
});

router.get('/cp', async function (req, res, next) {
    var id = req.query.id
    try {
        
        const market = await marketServices.getMarketCp(id)
        
        res.status(201).json({
            cp: market[0].postal_codes
        });
        
    } catch (error) {
        res.status(500).json({
            data : error
        });
    }
});

module.exports = router;