const express = require('express');
const router = express.Router();
const PacksServices = require('../../services/packages');

const packsServices = new PacksServices();

router.get('/', async function (req, res, next) {
    try {

        const packs = await packsServices.getPacks()

        res.status(201).json({
            data: packs
        });

    } catch (error) {
        res.status(500).json({
            data: error
        });
    }
});

router.post('/', async function (req, res, next) {
    const { body } = req.body;
    //console.log(body)
    try {
        const regpack = await packsServices.packsRegister(body);
        if (regpack) {
            res.status(201).json({
                message: 'package created'
            });
        }
        else {
            res.status(201).json({
                message: 'package no created'
            });
        }
    } catch (err) {
        next(err);
    }
});

router.post('/delete', async function (req, res, next) {
    const { uid } = req.body;
    console.log(uid);
    console.log(req.body)
    try {
        const packageExist = await packsServices.packExistence(uid)
        if (packageExist) {
            const deletePack = await packsServices.deletePack(uid);
            if (deletePack) {
                res.status(200).json({
                    message: 'package deleted'
                });
            }
            else {
                res.status(204).json({
                    message: 'package not deleted'
                });
                res.status(500).json({
                    message: 'Error del servidor'
                });
            }
        } else {
            res.status(204)
        }

    } catch (err) {
        next(err);
    }
});

module.exports = router;