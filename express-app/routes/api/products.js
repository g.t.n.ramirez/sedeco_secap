const express = require('express');
const router = express.Router();
const ProductServices = require('../../services/products');
const firebase = require('../../firebase/firebase')

const productServices = new ProductServices();

router.get('/unorganized', async function (req, res, next) {
    try {

        const products = await productServices.getProduct()

        res.status(200).json({
            data: products
        });

    } catch (error) {
        res.status(500).json({
            data: error
        });
        console.log(error)
    }

})

router.get('/', async function (req, res, next) {
    try {
        let fruits = []
        let meat = []
        let groceries = []
        let vegetables = []
        const products = await productServices.getProduct()

        products.forEach(product => {
            let category = product.info.product_category
            switch (category) {
                case "Carnicos":
                    meat.push(product)
                    break;
                case "Frutas":
                    fruits.push(product)
                    break;
                case "Abarrotes/Granos y Semillas":
                    groceries.push(product)
                    break;
                case "Verduras y Hortalizas":
                    vegetables.push(product)
                    break;
            }
        })

        res.status(200).json({
            data: [
                { category: "Carnicos", products: meat },
                { category: "Frutas", products: fruits },
                { category: "Abarrotes/Granos y Semillas", products: groceries },
                { category: "Verduras y Hortalizas", products: vegetables }
            ]
        });

    } catch (error) {
        res.status(500).json({
            data: error
        });
        console.log(error)
    }
});

//Create products
router.post('/', async function (req, res, next) {
    const { jsonProduct } = req.body;

    try {
        const regProduct = await productServices.productsRegister(jsonProduct);
        if (regProduct) {
            res.status(200).json({
                message: 'product created'
            });
        }
        else {
            res.status(204).json({
                message: 'product no created'
            });
            res.status(500).json({
                message: 'Error del servidor'
            });
        }
    } catch (err) {
        next(err);
    }
});

router.post('/delete', async function (req, res, next) {
    const { uid } = req.body;
    console.log(uid);
    console.log(req.body)
    try {
        const productExist = await productServices.productExistence(uid)
        if (productExist) {
            const deleteProduct = await productServices.deleteProduct(uid);
        if (deleteProduct) {
            res.status(200).json({
                message: 'product deleted'
            });
        }
        else {
            res.status(204).json({
                message: 'product not deleted'
            });
            res.status(500).json({
                message: 'Error del servidor'
            });
        }
        } else {
            res.status(204)
        }
        
    } catch (err) {
        next(err);
    }
});

//Update products
router.put('/', async function (req, res, next) {
    const { uid, data } = req.body
    try {
        const productExist = await productServices.productExistence(uid)
        if (productExist) {
            const editedProduct = await productServices.editProduct(uid, data)
            res.status(200).json({
                edited: true
            })
        } else {
            res.status(204)
        }
    } catch (error) {
        res.status(500).json({ error: error })
    }
});

// router.get('/existencia/:uid', async function(req, res, next){
//     const { uid } = req.params
//     console.log(uid)
//     try {
//         const exists = await productServices.productExistence(uid);
//         res.status(200).json({existe:exists});
//     } catch (error) {
//         res.status(500).json({error:error})
//     }
// })

//Delete products

module.exports = router;