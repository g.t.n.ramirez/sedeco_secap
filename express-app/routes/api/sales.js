const express = require('express');
const router = express.Router();
const SalesServices = require('../../services/sales');
const UsersServices = require('../../services/users')
const MarketServices = require('../../services/markets')
const checkIfAuthenticated = require('../../middlewares/auth')

const salesServices = new SalesServices();
const usersServices = new UsersServices()
const marketServices = new MarketServices()

//checkIfAuthenticated -> if is a current user authenticated
//consulta de compras
router.post('/get', async function (req, res, next) {
    try {
        /*
        cps: json cpn arreglo de codigos postales con el siguiente formato
        {
    "cps": ["58000", "58001"]
        }
        */
       //cps=codigos postales
        const { cps } = req.body;
        console.log(cps)

        const sales = await salesServices.getSales(cps)

        res.status(201).json({
            data: sales
        });

    } catch (error) {
        res.status(500).json({
            data : error
        });
        console.log(error)
    }
});

//register
router.post('/', checkIfAuthenticated, async function (req, res, next) {
    try {
        const { sales } = req.body;
        const uid = req.authId
        const user = await usersServices.getUser(uid)
        let userData = user
        userData.id = uid
        const market = await marketServices.getMarketByPC(userData.postal_code)
        let today = new Date()
        let marketName = market.data().market_name
        let saleToSave = {
            client : userData,
            market : marketName,
            packages : sales[0].packages,
            products : sales[0].products,
            status : "En proceso",
            total : sales[0].cost,
            date : today,
            vehicle: ""
        }


        const sold = await salesServices.salesRegister(saleToSave)

        if(sold){
            console.log('Simón')
            res.status(200).json({
                message: 'successful sale'
            });
        }
        else{
            res.status(204).json({
                message: 'failed sale'
            });
        }

    } catch (error) {
        console.log(error)
        res.status(500).json({
            message : 'Ocurrió un error inesperado favor de ponerse en contacto con el equipo de desarrollo'
        });
    }
});

router.get('/', async function (req, res, next) {
    try {

        const sales = await salesServices.getAllSales()

        res.status(201).json({
            data: sales
        });

    } catch (error) {
        res.status(500).json({
            data : error
        });
    }
});

module.exports = router;
