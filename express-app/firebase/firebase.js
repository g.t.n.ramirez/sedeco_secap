const { config } = require('../config');
const admin = require('firebase-admin');
const serviceAccount = require('../serviceClient.json');
const firebase = require("firebase/app");
require("firebase/auth");
require("firebase/storage")
require("firebase/firestore");

const API_KEY = encodeURIComponent(config.apiKey)
const AUTH_DOMAIN = encodeURIComponent(config.authDomain)
const DATABASE_URL = encodeURIComponent(config.dataBaseUrl)
const PROJECT_ID = encodeURIComponent(config.projectId)
const STORAGE_BUCKET = encodeURIComponent(config.storageBucket)
const MESSAGING_SENDER_ID = encodeURIComponent(config.messaginSenderId)
const APP_ID = encodeURIComponent(config.appId)
const MEASUREMENT_ID = encodeURIComponent(config.measurementId)

var firebaseConfig = {
    apiKey: API_KEY,
    authDomain: AUTH_DOMAIN,
    databaseURL: DATABASE_URL,
    projectId: PROJECT_ID,
    storageBucket: STORAGE_BUCKET,
    messagingSenderId: MESSAGING_SENDER_ID,
    appId: APP_ID,
    measurementId: MEASUREMENT_ID
  };

  // Initialize Firebase
  const firebaseApp = firebase.initializeApp(firebaseConfig);

  const adminApp = admin.initializeApp({
    credential : admin.credential.cert(serviceAccount),
    storageBucket: STORAGE_BUCKET
  })

  module.exports = {
    firebase: firebaseApp, admin: adminApp};
