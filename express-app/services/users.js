const { firebase, admin } = require('../firebase/firebase')

class UsersServices {
    constructor(){
        this.collection = "users"
        this.db = firebase.firestore()
    }

    async getUser(uid){
        const user = await this.db.collection(this.collection).doc(uid).get()
        if (user.exists)
            return user.data()
        else
            return false
    }
    async createEmployee(email, password, data){
        try{
            const user = await admin.auth().createUser({
            email : email,
            password : password,
            }).catch(error =>console.log(error))
            admin.auth().setCustomUserClaims(user.uid, {role : data.role}).then( _ =>{
                firebase.firestore().collection("users").doc(user.uid).set(data)
            })
            return true
        } catch(error){
            return false
        }
    }
}

module.exports = UsersServices