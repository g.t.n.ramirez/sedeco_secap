const {firebase} = require('../firebase/firebase')
global.XMLHttpRequest = require("xhr2");


class PacksServices {
    constructor() {
        this.collection = 'packages';
        this.db = firebase.firestore()
    }

    async getPacks() {
        let obj = []
        try {
            //this.prodRegister()
            const storage = firebase.storage();
            const packs = await this.db.collection('packages').get();
            for (const pack of packs.docs) {
                let pathReference = await storage.ref(pack.data().photo).getDownloadURL()
                obj.push({
                    id : pack.id,
                    info: pack.data(),
                    photoUrl: pathReference
                })
            }
                
            
            return obj
        } catch (error) {
            console.log(error)
        }
    }

    async packsRegister(data) {
        let flag = false
        let addProduct = await this.db.collection('packages').add(data).then(ref => {
            console.log('Added document with ID: ', ref.id);
            flag = true
        });
        console.log('flag ', flag)
        return flag;
    }

    async deletePack(uid){
        await this.db.collection("packages").doc(uid).delete();
        return true;
    }

    async packExistence(uid){
        try {
            let pack = await this.db.collection('packages').doc(uid).get()
            if(pack.exists)
                return true
            return false
        } catch (error) {
            return false
        }
        
        
    }

}

module.exports = PacksServices;

/*
Registro de paquetes (formato)
json = {
    cost: '',
    name: '',
    products: []
}

products.push({
    category: '',
    id: '',
    image: '',
    name: '',
    quantity: ''
})



Registro de productros (formato)
json =  {
    cost: '',
    photo: '',
    product_category: '',
    product_name: '',
    quantity: '',
    sku: ''
}


Registrar una venta:

*/