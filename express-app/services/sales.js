const { firebase } = require('../firebase/firebase')

class SalesServices {
    constructor() {
        this.collection = 'sales';
        this.db = firebase.firestore()
    }

    async getSalesClient(idClien) {
        const salesFilter = await this.db.collection("sales")
            .where(idClien, "==", "client").get()

        var obj = [];
        salesFilter.forEach(doc => {
            obj.push(doc.id)
            obj.push(doc.data())
        })
        return obj;
    }

    async getSales(arr) {
        const salesFilter = await this.db.collection("sales")
            .where("postal_code", "in", arr).get()

        const sales = await this.db.collection('sales').get();

        var obj = [];
        salesFilter.forEach(doc => {
            obj.push(doc.id)
            obj.push(doc.data())
        })
        return obj;
    }

    async getAllSales() {
        const sales = await this.db.collection('sales').get();

        var obj = [];
        for (const sale of sales.docs) {
            obj.push({
                id: sale.id,
                info: sale.data(),
            })
        }
        return obj;
    }

    async salesRegister(data) {

        let flag = false
        let addProduct = await this.db.collection('sales').add(data).then(async _ => {
            let userReference = this.db.collection('users').doc(data.client.id)
            await userReference.update( {last_purchase : data.date} )
            flag = true
        });
        return flag;
    }
}

module.exports = SalesServices;