const {firebase} = require('../firebase/firebase')

class MarketServices {
    constructor() {
        this.collection = 'markets';
        this.db = firebase.firestore()
    }

    async getMarkets() {
        const markets = await this.db.collection('markets').get();
        var obj = [];
        markets.forEach(doc => {
            obj.push(doc.id)
        })
        return obj;
    }

    async getMarketCp(id) {
        var obj = [];
        const markets = await this.db.collection('markets').doc(`${id}`).get().then(function(doc) {
            if (doc.exists) {
                obj.push(doc.data())
            } else {
                // doc.data() will be undefined in this case
                console.log("No such document!");
            }
        }).catch(function(error) {
            console.log("Error getting document:", error);
        });
        return obj;
    }

    async getMarketByPC(postal_code){
        console.log(postal_code)
        const markets = await this.db.collection('markets').where("postal_codes", "array-contains", postal_code).get()
        if(!markets.empty){
            return markets.docs[0]
        }
        return false
    }
}

module.exports = MarketServices;