const {firebase} = require('../firebase/firebase')
global.XMLHttpRequest = require("xhr2");

class ProductServices {
    constructor() {
        this.collection = 'products';
        this.db = firebase.firestore()
    }

    async getProduct() {
        const products = await this.db.collection('products').get();
        const storage = firebase.storage();
        
        var obj = [];
        for (const product of products.docs) {
            let pathReference = await storage.ref(product.data().photo).getDownloadURL()
            obj.push({
                id:product.id,
                info:product.data(),
                productInCart:0,
                photoUrl: pathReference
            })
        }
        return obj;
    }

    async productsRegister(data) {
        let flag = false
        let addProduct = await this.db.collection('products').add(data).then(ref => {
            console.log('Added document with ID: ', ref.id);
            flag = true
        });
        console.log('flag ', flag)
        return flag;
    }

    async editProduct(uid, newData){
        let productReference = this.db.collection('products').doc(uid)
        await productReference.update(newData)
        let editedProduct = await productReference.get();
        return editedProduct
    }

     async deleteProduct(uid){
        await this.db.collection("products").doc(uid).delete();
        return true;
    }

    async productExistence(uid){
        try {
            let product = await this.db.collection('products').doc(uid).get()
            if(product.exists)
                return true
            return false
        } catch (error) {
            return false
        }
        
        
    }
}

module.exports = ProductServices;